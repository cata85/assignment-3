from functools import reduce


def print_string(m, n): print(m * n)


def biggest(a, b): return a if a > b else b


def mirror(num): return int(str(num)[::-1])


def swap_last_first(num): return str(num)[-1:] + str(num)[1:-1] + str(num)[:1]


def is_palindrome(num): return str(num) == str(num)[::-1]


def factorial(num): return 1 if num == 1 else num * factorial(num-1)


def sum_list(num_list): return num_list[0] if len(num_list) == 1 else num_list[0] + sum_list(num_list[1:])


def smallest(*args): return reduce((lambda x, y: x if x < y else y), args)


def union(list_one, list_two): return list(filter((lambda x: x in list_two), list_one))


def make_odds(num_size, num_start): return list(filter(lambda x: x % 2 == 1, list(map(lambda x: x + num_start, range(num_size * 2)))))


print_string("hello", 5)
print biggest(1, 2)
print mirror(45379)
print swap_last_first(12345)
print is_palindrome(11511)
print factorial(4)
print sum_list([1, 2, 3, 4])
print smallest(5, 3, 7, 2)
print union([1, 2, 3, 4, 5, 6, 7], [2, 5, 1, 8, 9, 4])
print make_odds(5, 11)
